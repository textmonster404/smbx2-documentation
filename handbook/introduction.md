![A user’s guide to SMBX2 b4](images/image42.png)

# Introduction

SMBX2 (2.0.0 b4, henceforth referred to as SMBX2 b4) is the latest
stable build of SMBX2, as of January 2020. It is a direct update to the
last preview build 2.0.0 b4 p2 (a.k.a. SMBX2 PAL), and fixes many of the
issues present in that version. As
such, it is stable for use creating episodes as well as levels.

Please refer to the Encountering
Bugs segment below for how to proceed if you
encounter anything out of the ordinary.
