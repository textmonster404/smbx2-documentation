# Weather effect constants

Used to describe section weather effects.

| Constant | Value |
| --- | --- |
| WEATHER_NONE | 0 |
| WEATHER_RAIN | 1 |
| WEATHER_SNOW | 2 |
| WEATHER_FOG | 3 |
| WEATHER_SANDSTORM | 4 |
| WEATHER_CINDERS | 5 |
| WEATHER_WISPS | 6 |